package com.example.googlemaplerning.evgeny.conductorlerning

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import kotlinx.android.synthetic.main.controller_second.view.*


class SecondController : Controller {


    //Если мы просто добавим конструктор, то. в случае уничтожения нашего контроллера,
// Conductor его пересоздаст, и мы потеряем информацию о наших шишках.
// Чтобы этого избежать нужно записать наши данные в args. Args сохраняются
// кондуктором при уничтожении контроллера.
    constructor(bundle: Bundle) : super(bundle)

    //нужен обязательно
    constructor() : super()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val view = inflater.inflate(R.layout.controller_second, container, false)
        Log.d("SecondController", "onCreateView")

        view.contentSecond.setOnClickListener {

        }

        view.backbackButtons.setOnClickListener {
            router.popCurrentController()
        }

        return view
    }

}