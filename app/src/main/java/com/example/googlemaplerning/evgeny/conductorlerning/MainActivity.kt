package com.example.googlemaplerning.evgeny.conductorlerning

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    lateinit var router: Router


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //привязывает роутер к нашей активити и к её жизненному циклу.
        router = Conductor.attachRouter(this, parentFill, savedInstanceState)


        // Поэтому если активити не была создана заново, а была восстановлена,
        // то и установка корневого контроллера не требуется, т.к. он был воссоздан
        // из сохранённого состояния.

        //Ничего не мешает нам убрать эту проверку, но тогда каждый раз при
        // создании активити у нас будет создаваться новый контроллер и мы потеряем
        // все сохранённые данные.
        if (!router.hasRootController()) {
            val controller = RotController()

            //Стоит обратить внимание на то, что при удалении корневого контроллера
            // его вид не удаляется со сцены. Это оставлено для того, чтобы при закрытии
            // нашей активити мы могли наблюдать “качественную” анимацию закрытия.
            router.setRoot(RouterTransaction.with(controller))
        }
    }


    override fun onBackPressed() {

        //если handleBack вернул false, хозяин этого роутера должен быть уничтожен.
        if (!router.handleBack()) {
            super.onBackPressed();
        }
    }


}
