package com.example.googlemaplerning.evgeny.conductorlerning

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import kotlinx.android.synthetic.main.controller_root.view.*

class RotController : Controller {

    //ссылки на view нужно обнулять в onDestroyView для того что бы не было утечки
    //памяти

    //Жизненный цикл контроллеров привязан к фрагменту в активити.


    //поля контроллера сохраняют свою ссылку при перевороте или попадание в бекстек
    var peremenia = false

    var content = ""

    //нужен обязательно
    constructor() : super()


    //вызывается при создании
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        val view = inflater.inflate(R.layout.controller_root, container, false)
        Log.d("RotController", "onCreateView $peremenia")

        view.button_main.setOnClickListener {
            view.txvContent.text = "Content New"
            peremenia = true
        }

        view.buttonNerxnNext.setOnClickListener {
            val controller = SecondController()
            router.pushController(RouterTransaction.with(controller))
        }

        return view
    }


    //вызывается каждый раз когда пользователь видит экран
    override fun onAttach(view: View) {
        super.onAttach(view)
        Log.d("RotController", "onAttach $peremenia")
        view.txvContent.text = content
    }


    override fun onSaveInstanceState(outState: Bundle) {
        Log.d("RotController", "onSaveInstanceState")
        outState.putString("TextView1", view?.txvContent?.text.toString())
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val str = savedInstanceState.getString("TextView1", "Nothing")
        Log.d("RotController", "onRestoreInstanceState 1 argument $str")
    }


    override fun onDestroyView(view: View) {
        super.onDestroyView(view)
        Log.d("RotController", "onDestroyView")
    }


    override fun onDetach(view: View) {
        super.onDetach(view)
        Log.d("RotController", "onDetach")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("RotController", "onDestroy")
    }


    //    //TODO не восстанавливает состояние view РАЗОБРАТЬСЯ
    override fun onRestoreViewState(view: View, savedViewState: Bundle) {
        super.onRestoreViewState(view, savedViewState)
        Log.d("RotController", "onRestoreInstanceState 2 argument")
        val str = savedViewState.getString("TextView1", "Nothing")
        view.txvContent.text = str
    }


}
